const { Vehicle } = require('../models')
const imagekit = require('../lib/imageKit')
const { Op } = require("sequelize")

const createVehicle = async(req, res) => {
    const { name, price, size, image, stock, warehouseId } = req.body
    try {
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, 
            fileName: `${req.file.originalname}.${ext}`, 
        })
        console.log(img.url)

        const newVehicle = await Vehicle.create({
            name,
            price,
            size,
            image: img.url,
            stock,
            warehouseId
        })

        res.status(201).json({
            status: 'success',
            data: {
                newVehicle
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findVehicle = async(req, res) => {
    try {
        if(req.query){
            vehicle = await Vehicle.findAll({
                where: {
                    [Op.or]: [
                        {name: {[Op.like]: req.query.name ? req.query.name.charAt(0).toUpperCase() + req.query.name.slice(1):null }},
                        {size: req.query.size? req.query.size:null}
                    ]
                    
                }
            })
            res.status(200).json({
                status: 'Success',
                data: {
                    vehicle
                }
            })
        } else {
            vehicle = await Vehicle.findAll()
            res.status(200).json({
                status: 'Success',
                data: {
                    vehicle
                }
            })
        }
        
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const editVehicle = async(req, res) => {
    try {
        const { name, price, capacity } = req.body
        const id = req.params.id
        const vehicle = await Vehicle.update({
            name,
            price,
            capacity,
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                name,
                price,
                capacity,
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteVehicle = async(req, res) => {
    try {
        const id = req.params.id
        await Vehicle.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `Kendaraan dengan id ${id} telah dihapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createVehicle,
    findVehicle,
    editVehicle,
    deleteVehicle,
}