const { Warehouse, Vehicle } = require('../models')

const createWarehouse = async(req, res) => {
    const { name, address, owner } = req.body
    try {
        const newWarehouse = await Warehouse.create({
            name,
            address,
            owner
        })

        res.status(201).json({
            status: 'success',
            data: {
                newWarehouse
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findWarehouses = async (req, res) => {
    try {
        const warehouses = await Warehouse.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                warehouses
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findWarehouseById = async (req, res) => {
    try {
        const warehouse = await Warehouse.findOne({
            where: {
                id: req.params.id
            },
            include: {
                model: Warehouse,
                attributes: ['id', ['name', 'namaToko'], 'stock'] // name = nama colomnya, namaToko = new value as nama colomn name
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                warehouse
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findVehicle = async(req, res) => {
    try {
        if(req.query){
            vehicle = await Vehicle.findAll({
                where: {
                    [Op.or]: [
                        {name: {[Op.like]: req.query.name ? req.query.name.charAt(0).toUpperCase() + req.query.name.slice(1):null }},
                        {size: req.query.size? req.query.size:null}
                    ]
                    
                }
            })
            res.status(200).json({
                status: 'Success',
                data: {
                    vehicle
                }
            })
        } else {
            vehicle = await Vehicle.findAll()
            res.status(200).json({
                status: 'Success',
                data: {
                    vehicle
                }
            })
        }
        
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const editWarehouse = async(req, res) => {
    try {
        const { name, address, owner } = req.body
        const id = req.params.id
        const warehouse = await Warehouse.update({
            name,
            address,
            owner,
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                name,
                address,
                owner,
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteWarehouse = async(req, res) => {
    try {
        const id = req.params.id
        await Warehouse.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `Gudang dengan id ${id} telah dihapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createWarehouse,
    findWarehouses,
    findWarehouseById,
    findVehicle,
    editWarehouse,
    deleteWarehouse,
}