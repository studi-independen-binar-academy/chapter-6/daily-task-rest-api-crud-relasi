const router = require('express').Router()
const Vehicle = require('../controller/vehicleController')
const Warehouse = require('../controller/warehouseController')
const Admin = require('../controller/admin/vehicleController')
const Upload =  require('../controller/uploadController')

// middleware
const uploader = require('../middlewares/uploader')

// admin client side
router.get('/admin', Admin.homepage)
router.get('/admin/vehicles', Admin.dataPage)
router.get('/admin/add', Admin.createPage)
router.post('/admin/add', uploader.single('image'), Admin.createVehicle)
router.get('/admin/edit/:id', Admin.editPage)
router.post('/admin/edit/:id', Admin.editVehicle)
router.post('/admin/delete/:id', Admin.deleteVehicle)
router.get('/admin/vehicles/filter', Admin.findVehicle)

// API server
router.post('/api/vehicle', uploader.single('image'), Vehicle.createVehicle)
router.get('/api/vehicle', Vehicle.findVehicle)
router.put('/api/vehicle/:id', Vehicle.editVehicle)
router.delete('/api/vehicle/:id', Vehicle.deleteVehicle)

router.post('/api/warehouses', Warehouse.createWarehouse)
router.get('/api/warehouses', Warehouse.findWarehouses)
router.get('/api/warehouses/:id', Warehouse.findWarehouseById)
router.put('/api/warehouses/:id', Warehouse.editWarehouse)
router.delete('/api/warehouses/:id', Warehouse.deleteWarehouse)
module.exports = router