'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Vehicle extends Model {
        static associate(models) {
        }
    }
    Vehicle.init({
        name: DataTypes.STRING,
        price: DataTypes.INTEGER,
        size: DataTypes.STRING,
        image: DataTypes.STRING,
        stock: DataTypes.INTEGER,
        warehouseId: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'Vehicle',
    });

    Vehicle.associate = function(models){
        Vehicle.belongsTo(models.Warehouse, {
            foreignKey: 'warehouseId'
        })
    }
    
    return Vehicle;
};